#include <iostream>

#include "Complex.h"

int main() {
    Complex* a = new Complex(5.1, 4.3);
    Complex* b = new Complex(6.5, 1.2);

    std::cout << *a << " -- " << *b << std::endl;

    std::cout << *a + *b << std::endl;


    delete a;
    delete b;

    return 0;
}