public class PPMain {
    public static void main(String[] args) throws OutOfInkException, OutOfSpaceExeption {
        Paper paper = new Paper();
        Pen pen = new Pen();

        pen.write(paper, "Hello, world!");
        paper.show();
    }
}