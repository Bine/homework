#include <iostream>
#include <ctype.h>

#include "Date.h"

class InvalidParam {
    public:
        std::string text;
        InvalidParam(const std::string& text) : text(text) {}
};


class Passport {
private:
    std::string firstName;
    std::string SecondName;
    std::string mySerial;
    int year;
    int month;
    int day;
    int myNumber;
    Date dat;
public:
    static int number;
    static std::string serial;
public:
    Passport(std::string firstName, std::string SecondName, int year, int month, int day) {
        this->firstName = firstName;
        this->SecondName = SecondName;
        this->year = year;
        this->month = month;
        this->day = day;
        this->dat = new Date(this->day, this->month, this->year);

        if ( number >= 1000000 ) {
            number = 100001;
            if ( serial[1] == 'Z' ) {
                serial[0] += 1;
                serial[1] = 'A';
            } else {
            serial[1] += 1;
            }
        } 
        myNumber = number;
        mySerial = serial;
        number += 1;
    }
    ~Passport(){

    }


    void newSerial(std::string newSerial = "AA", int newNumber = 100000 ) {
        if ( newSerial.length() > 2 || newSerial.length() < 2 ) {
            throw InvalidParam("InvalidParam");
        }
        if ( !isalpha(newSerial[0]) || !isalpha(newSerial[1]) ) {
            // std::cout << isalpha(newSerial[0]) << std::endl;
            throw InvalidParam("InvalidParam");
        } else {
            newSerial[0] = toupper(newSerial[0]);
            newSerial[1] = toupper(newSerial[1]);
        }
        if ( newNumber >= 1000000 || newNumber <= 100000 ) {
            throw InvalidParam("InvalidParam");
        }
        serial = newSerial;
        number = newNumber;
    }


    std::string getFirstName() {
        return this->firstName;
    }

    std::string getSecondName() {
        return this->SecondName;
    }

    int getMyNumber() {
        return this->myNumber;
    }

    std::string getMySerial() {
        return this->mySerial;
    }

    int getYear() {
        return this->year;
    }

    int getMonth() {
        return this->month;
    }

    int getDay() {
        return this->day;
    }

};

int Passport::number = 100001;
std::string Passport::serial = "AA";

std::ostream& operator<<(std::ostream& out, Passport& passport) {
return out  << "    PASSPORT UKRAINE\n"
            << "\n        " << passport.getMySerial() << ' ' << passport.getMyNumber()
            << "\n    SecondName: " << passport.getSecondName()
            << "\n    FirstName: " << passport.getFirstName()
            << "\n    Born: " << passport.getDay() << '.' << passport.getMonth() << '.' << passport.getYear()
            << std::endl;
}


int main() {
    Passport* Ivan = new Passport("Ivan", "Kuldarac", 1994, 12, 20);
    Ivan->newSerial("tt", 999999);
    Passport* Dima = new Passport("Dima", "Ksardax", 1943, 8, 10);
    Passport* Vasia = new Passport("Vasia", "Milton", 1932, 10, 1);
    Passport* Loki = new Passport("Vasia", "Milton", 1932, 10, 1);

    std::cout << *Ivan << std::endl;
    std::cout << *Dima << std::endl;
    std::cout << *Vasia << std::endl;
    std::cout << *Loki << std::endl;

    return 0;
}